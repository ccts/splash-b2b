// Gulp
var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');

//Browser Sync

var browserSync = require('browser-sync').create();

// Sync Task

gulp.task('fullSync', function(){
	browserSync.init({
		server: "./app"
	});
	gulp.watch('src/css/*.scss', ['sass']);
	gulp.watch('src/js/*.js', ['uglify']);
	gulp.watch('app/*.html').on('change', browserSync.reload);
});

// Functions

gulp.task('hello', function(){
	console.log('Hello World');
});

gulp.task('sass', function(){
	return gulp.src('src/css/*.scss')
	.pipe(sass({outputStyle: "compressed"}).on('error', sass.logError))
	.pipe(gulp.dest('app/css'))
	.pipe(browserSync.stream())
});

gulp.task('uglify', function(){
	return gulp.src('src/js/*.js')
	.pipe(uglify())
	.pipe(gulp.dest('app/js'))
	.pipe(browserSync.stream())
});

// Imagemin
gulp.task('imagemin', function(){
	return gulp.src('src/images/*')
	.pipe(imagemin({progressive: true}))
	.pipe(gulp.dest('app/images'))
});