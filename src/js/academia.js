/*
// Academia JS
//
*/

// Declare Vars
var logo = document.querySelector('.logo');
var siteTitle = document.querySelector('.site-title');
var info = document.querySelector('.info');
var knowMore = document.querySelector('.know-more');
var footer = document.querySelector('.footer');

function animateAll() {
	setTimeout(function(){
		logo.classList.add('animated', 'fadeIn');
	}, 500);

	setTimeout(function(){
		siteTitle.classList.add('animated', 'fadeIn');
	}, 2000);

	setTimeout(function(){
		info.classList.add('animated', 'fadeIn');
		knowMore.classList.add('animated', 'fadeIn');
		footer.classList.add('animated', 'fadeIn');
	}, 2250);
}
animateAll();

// Latest Sermon Grab

function getSermons() {
	var xml = new XMLHttpRequest();
	xml.overrideMimeType('text/xml');
	xml.open('GET', 'http://chronicles.cccm.com/assets/back-to-basics.xml', true);
	xml.onreadystatechange = function() {
		if(xml.readyState == 4 && xml.status == 200) {
			var response = xml.responseXML;
			var latestStudy = response.getElementsByTagName('item')[response.getElementsByTagName('item').length - 1];
			// 13 15 17
			var latestResponse = document.getElementById('latestStudy');
			latestResponse.innerHTML = '<div class="card-title"><h3>' + latestStudy.childNodes[1].textContent + '</h3></div><div class="card-content"><div class="study-date"><h4>Date:</h4><p>'
			+ latestStudy.childNodes[15].textContent.slice(0, -13) + '</p></div><div class="study-dur"><h4>Duration:</h4><p>' + latestStudy.childNodes[17].textContent + '</p></div><div class="card-button"><button class="button red"><a href="' + latestStudy.childNodes[13].textContent + '">Download</a></button></div></div>'			;
		} else {

		}
	}
	xml.send(null);
}
getSermons();